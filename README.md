# WordPress Mailing List Manager

(c) E.I.C.C., Inc.
All Rights Reserved
Released under the MIT license

The point of this plugin is to give WordPress a simple mailing list manager. You can use it to send email through the normal WordPress STMP system.

## Features I'd like to see
- Send to everyone
- Send to a specific role
- Send via SMTP
- Send via API call