<?php

/*
 * Plugin Name: WordPress Mailing List
 * Plugin URI:  https://gitlab.com/calevans/tide-tables-plugin
 * Description: Basic Mailing list for WordPress
 * Version:     1.0.0
 * Author:      Cal Evans
 * Author URI:  https://calevans.com
 * Text Domain: wp-mailing-list
 * License:     MIT
 */
if (! defined('WPINC')) {
    die();
}

require_once '/app/vendor/autoload.php';
use WPMailingList\Plugin;

(new Plugin())->init();
